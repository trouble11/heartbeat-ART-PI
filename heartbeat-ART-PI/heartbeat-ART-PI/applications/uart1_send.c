/*
 * Copyright (c) 2006-2020, RT-Thread Development Team
 *
 * SPDX-License-Identifier: Apache-2.0
 *
 * Change Logs:
 * Date           Author       Notes
 * 2020-09-02     RT-Thread    first version
 */

#include <rtthread.h>
#include <rtdevice.h>
#include "drv_common.h"


#define ADC_DEV_NAME        "adc1"  /* ADC 设备名称 */
#define ADC_DEV_CHANNEL     3       /* ADC 通道 */
#define REFER_VOLTAGE       330         /* 参考电压 3.3V,数据精度乘以100保留2位小数*/
#define CONVERT_BITS        (1 << 16)   /* 转换位数为12位 */


#define SAMPLE_UART_NAME       "uart1"    /* 串口设备名称 */
static rt_device_t serial;                /* 串口设备句柄 */
struct serial_configure config = RT_SERIAL_CONFIG_DEFAULT;  /* 初始化配置参数 */

char str[] = "add 1,0,";
char end[] = {0xFF,0xFF,0xFF};
char data[] = {'1','1','1'};
int uart_send(void)
{

    rt_adc_device_t adc_dev;            /* ADC 设备句柄 */
    rt_uint32_t value;
    rt_uint32_t vol;
    /* 查找设备 */
    adc_dev = (rt_adc_device_t)rt_device_find(ADC_DEV_NAME);
    if (adc_dev == RT_NULL)
    {
        rt_kprintf("adc sample run failed! can't find %s device!\n", ADC_DEV_NAME);
        return RT_ERROR;
    }
    /* 使能设备 */
    rt_adc_enable(adc_dev, ADC_DEV_CHANNEL);

    /* step1：查找串口设备 */
    serial = rt_device_find(SAMPLE_UART_NAME);

    /* step2：修改串口配置参数 */
    config.baud_rate = BAUD_RATE_115200;        //修改波特率为 115200
    config.data_bits = DATA_BITS_8;           //数据位 8
    config.stop_bits = STOP_BITS_1;           //停止位 1
    config.bufsz     = 128;                   //修改缓冲区 buff size 为 128
    config.parity    = PARITY_NONE;           //无奇偶校验位

    /* step3：控制串口设备。通过控制接口传入命令控制字，与控制参数 */
    rt_device_control(serial, RT_DEVICE_CTRL_CONFIG, &config);

    /* step4：打开串口设备。以中断接收及轮询发送模式打开串口设备 */
    rt_device_open(serial, RT_DEVICE_FLAG_INT_RX);

    while(1)
    {
        /* 读取采样值 */
        value = rt_adc_read(adc_dev, ADC_DEV_CHANNEL);

        rt_device_write(serial, 0, str, (sizeof(str) - 1));

        data[0] = (rt_uint8_t)(value>>8)/100 + 0x30;
        data[1] = (rt_uint8_t)((value>>8)/10)%10 + 0x30;
        data[2] = (rt_uint8_t)(value>>8)%10 + 0x30;

        rt_device_write(serial, 0, data, 3);

        rt_device_write(serial, 0, end, 3);
        rt_thread_mdelay(5);

    }
    return RT_EOK;
}
MSH_CMD_EXPORT(uart_send,uart_send);

